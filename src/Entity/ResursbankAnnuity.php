<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\PartPayment\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use InvalidArgumentException;
use JsonException;

/**
 * NOTE: This class is named in a weird way to make sure it works with
 * Prestashop's internal Entity -> Table mapping
 * (Entity model -> _DB_PREFIX_ . [entity model class in snake casing]).
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Resursbank\PartPayment\Repository\ResursbankAnnuityRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class ResursbankAnnuity
{
    public const KEY_ANNUITY_ID = 'annuity_id';
    public const KEY_METHOD_ID = 'method_id';
    public const KEY_FACTOR = 'factor';
    public const KEY_DURATION = 'duration';
    public const KEY_RAW = 'raw';
    public const KEY_TITLE = 'title';


    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer", name="annuity_id", nullable=false, scale=5, options={"unsigned":true})
     */
    private $annuityId;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", name="method_id", nullable=false, scale=5, options={"unsigned":true})
     */
    private $methodId;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", name="duration", nullable=false, scale=5, options={"unsigned":true})
     */
    private $duration;

    /**
     * @var int
     *
     * @ORM\Column(type="decimal", name="factor", precision=15, scale=5, nullable=false, options={"unsigned":true, "default":0.0})
     */
    private $factor;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=false, length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $raw;

    /**
     * @var string
     */
    private $rawJson;

    /**
     * @var DateTime
     *
     * @ORM\Column(type="datetime", name="created_at", nullable=false)
     */
    private $createdAt;

    /**
     * @var DateTime
     *
     * @ORM\Column(type="datetime", name="updated_at", nullable=false)
     */
    private $updatedAt;

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps(): void
    {
        $timestamp = new DateTime();

        $this->setUpdatedAt($timestamp);

        if ($this->getCreatedAt() === null) {
            $this->setCreatedAt($timestamp);
        }
    }

    /**
     * @return int
     */
    public function getAnnuityId(): int
    {
        return $this->annuityId;
    }
    
    /**
     * @return int
     */
    public function getMethodId(): int
    {
        return $this->methodId;
    }

    /**
     * @param int $methodId
     * @return ResursbankAnnuity
     */
    public function setMethodId(int $methodId): ResursbankAnnuity
    {
        if ($methodId < 0) {
            throw new InvalidArgumentException(
                'Method id may not be negative.'
            );
        }

        $this->methodId = $methodId;

        return $this;
    }

    /**
     * @return int
     */
    public function getDuration(): int
    {
        return $this->duration;
    }

    /**
     * @param int $duration
     * @return ResursbankAnnuity
     */
    public function setDuration(int $duration): ResursbankAnnuity
    {
        if ($duration < 0) {
            throw new InvalidArgumentException(
                'Duration value may not be negative.'
            );
        }

        $this->duration = $duration;

        return $this;
    }

    /**
     * @return float
     */
    public function getFactor(): float
    {
        return (float)$this->factor;
    }

    /**
     * @param float $factor
     * @return ResursbankAnnuity
     */
    public function setFactor(float $factor): ResursbankAnnuity
    {
        if ($factor < 0) {
            throw new InvalidArgumentException(
                'Factor value may not be negative.'
            );
        }

        $this->factor = $factor;

        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return $this
     *
     * @throws InvalidArgumentException
     */
    public function setTitle(
        string $title
    ): ResursbankAnnuity {
        if ($title === '') {
            throw new InvalidArgumentException('Title may not be empty.');
        }

        $this->title = $title;

        return $this;
    }

    /**
     * @return string
     */
    public function getRaw(): string
    {
        return $this->raw;
    }

    /**
     * @param $key
     * @return mixed
     * @throws JsonException
     */
    private function getRawJson($key)
    {
        if (empty($this->rawJson)) {
            $this->rawJson = json_decode($this->getRaw(), true, 512, JSON_THROW_ON_ERROR);
        }

        return $this->rawJson[$key] ?? '';
    }

    /**
     * @return string
     * @throws JsonException
     */
    public function getDescription(): string
    {
        return $this->getRawJson('description');
    }

    /**
     * @return mixed|string
     * @throws JsonException
     */
    public function getSpecificType(): string
    {
        return $this->getRawJson('specificType');
    }

    /**
     * CustomerType, used to be returned as a string or an array from Resurs depending on the number of types
     * supported. We always want this as an array.
     *
     * @return array
     * @throws JsonException
     */
    public function getCustomerType(): array
    {
        return (array)$this->getRawJson('customerType');
    }

    /**
     * @return mixed|string
     * @throws JsonException
     */
    public function getType(): string
    {
        return $this->getRawJson('specificType');
    }

    /**
     * @return mixed|string
     * @throws JsonException
     */
    public function getId(): string
    {
        return $this->getRawJson('id');
    }

    /**
     * @param string $raw
     *
     * @return $this
     */
    public function setRaw(
        string $raw
    ): ResursbankAnnuity {
        $this->raw = $raw;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt(): ?DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param DateTime $createdAt
     *
     * @return $this
     */
    public function setCreatedAt(
        DateTime $createdAt
    ): ResursbankAnnuity {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getUpdatedAt(): ?DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param DateTime $updatedAt
     *
     * @return $this
     */
    public function setUpdatedAt(
        DateTime $updatedAt
    ): ResursbankAnnuity {
        $this->updatedAt = $updatedAt;

        return $this;
    }
}
