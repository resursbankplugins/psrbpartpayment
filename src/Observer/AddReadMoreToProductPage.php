<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\PartPayment\Observer;

use Exception;
use InvalidArgumentException;
use function is_float;
use PrestaShop\PrestaShop\Adapter\Entity\Context;
use PrestaShop\PrestaShop\Adapter\Presenter\Product\ProductLazyArray;
use PrestaShop\PrestaShop\Core\Localization\Exception\LocalizationException;
use PrestaShopBundle\Translation\TranslatorComponent;
use psrbpartpayment;
use Resursbank\Core\Config\Config as CoreConfig;
use Resursbank\Core\Entity\ResursbankPaymentMethod;
use Resursbank\Core\Logger\LoggerInterface;
use Resursbank\Core\Repository\ResursbankPaymentMethodRepository;
use Resursbank\Core\Service\Filesystem;
use Resursbank\Core\Service\ReadMore;
use Resursbank\Ecommerce\Service\Translation;
use Resursbank\PartPayment\Config\Config;
use Resursbank\PartPayment\Entity\ResursbankAnnuity;
use Resursbank\PartPayment\Repository\ResursbankAnnuityRepository;

/**
 * Adds information about "Read more" links to product pages.
 */
class AddReadMoreToProductPage
{
    /**
     * @var Config
     */
    protected $config;

    /**
     * @var ResursbankPaymentMethodRepository
     */
    protected $methodRepository;

    /**
     * @var ResursbankAnnuityRepository
     */
    protected $annuityRepository;

    /**
     * @var Context
     */
    protected $context;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var CoreConfig
     */
    protected $coreConfig;

    /**
     * @var Translation
     */
    protected $translation;

    /**
     * @var \psrbpartpayment
     */
    private $module;

    /**
     * @param LoggerInterface $logger
     * @param Config $config
     * @param ResursbankPaymentMethodRepository $methodRepository
     * @param ResursbankAnnuityRepository $annuityRepository
     */
    public function __construct(
        LoggerInterface $logger,
        Config $config,
        ResursbankPaymentMethodRepository $methodRepository,
        ResursbankAnnuityRepository $annuityRepository,
        CoreConfig $coreConfig
    ) {
        $this->config = $config;
        $this->methodRepository = $methodRepository;
        $this->annuityRepository = $annuityRepository;
        $this->context = Context::getContext();
        $this->logger = $logger;
        $this->coreConfig = $coreConfig;
        $this->translation = new Translation();
        // Do not forget to handle ShopConstraint here.
        $this->translation->setLanguage($this->coreConfig->getCountry(null));
        $this->module = \Module::getInstanceByName('psrbpartpayment');
    }

    /**
     * @param psrbpartpayment $module
     * @param array $params
     *
     * @return string
     */
    public function execute(psrbpartpayment $module, array $params): string
    {
        $annuity = null;
        $result = '';

        try {
            $productPrice = $this->getProductPrice($params);

            if ($this->config->getEnabled()) {
                /** @var ResursbankAnnuity $annuity */
                $annuity = $this->annuityRepository->find(
                    $this->config->getAnnuityId()
                );
            }

            if ($annuity !== null) {
                /** @var ResursbankPaymentMethod $method */
                $method = $this->methodRepository->find($annuity->getMethodId());
                $startingPrice = ceil(round(
                    $productPrice * $annuity->getFactor()
                ));

                if ($method !== null &&
                    $this->isValid($annuity, $method, $productPrice)
                ) {
                    /** @var ReadMore $coreReadMore */
                    $coreReadMore = $module->get('resursbank.core.readmore');

                    $templateData = $coreReadMore->getTemplateData(
                        $this->coreConfig->getReadMoreTranslatedString($method),
                        '',
                        'psrbpartpayment-read-more',
                        $method,
                        '',
                        $this->getReadMoreInfoText(
                            $module,
                            $startingPrice,
                            $annuity
                        ),
                        $productPrice
                    );

                    $this->context->smarty->assign($templateData);

                    $result = $this->context->smarty->fetch(
                        Filesystem::getTemplate($module->name, 'read-more.tpl')
                    );
                }
            }
        } catch (Exception $e) {
            $this->logger->error(
                'Unable to add "Read More" widget to product page.'
            );
            $this->logger->exception($e);
        }

        return $result;
    }

    /**
     * @throws LocalizationException
     */
    protected function getFormattedCurrencyText(float $price): string
    {
        $iso_code = $this->context->currency->iso_code;

        return $this->context->currentLocale->formatPrice($price, $iso_code);
    }

    /**
     * @param ResursbankAnnuity $annuity
     * @param ResursbankPaymentMethod $method
     * @param float $price
     *
     * @return bool
     */
    protected function isValid(
        ResursbankAnnuity $annuity,
        ResursbankPaymentMethod $method,
        float $price
    ): bool {
        $calculatedPrice = $price * $annuity->getFactor();

        return
            ($calculatedPrice >= $this->config->getThreshold()) &&
            ($price >= $method->getMinOrderTotal()) &&
            ($price <= $method->getMaxOrderTotal())
        ;
    }

    /**
     * @throws LocalizationException
     * @throws Exception
     */
    protected function getReadMoreInfoText(
        psrbpartpayment $module,
        float $price,
        ResursbankAnnuity $annuity
    ): string {
        $formattedPrice = $this->getFormattedCurrencyText($price);
        $text = sprintf(
            'Starting at %s per month, for %s months',
            $formattedPrice,
            $annuity->getDuration()
        );

        return $this->module->l($text);
    }

    /**
     * @param array $params
     *
     * @return void
     */
    private function getProductPrice(array $params): float
    {
        if (!isset($params['product']) ||
            !$params['product'] instanceof ProductLazyArray
        ) {
            throw new InvalidArgumentException('No product supplied');
        }

        if (!isset($params['product']['price_amount']) ||
            !is_float($params['product']['price_amount'])
        ) {
            throw new InvalidArgumentException('Product price invalid');
        }

        return $params['product']['price_amount'];
    }

    /**
     * @param psrbpartpayment $module
     *
     * @return TranslatorComponent
     *
     * @throws Exception
     */
    private function getTranslator(psrbpartpayment $module): TranslatorComponent
    {
        $result = $module->getTranslator();

        if ($result === null) {
            throw new Exception('Translator does not exist.');
        }

        return $result;
    }
}
