<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\PartPayment\Exception;

use PrestaShopException;

class IntegrationException extends PrestaShopException
{
}
