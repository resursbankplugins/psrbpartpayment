<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\PartPayment\Repository;

use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Collections\ExpressionBuilder;
use Doctrine\ORM\EntityRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Resursbank\PartPayment\Entity\ResursbankAnnuity;
use Resursbank\PartPayment\Exception\ResolveCriteriaException;

/**
 * Repository implementation of the Resurs Bank Payment Method table.
 */
class ResursbankAnnuityRepository extends EntityRepository
{
    /**
     * @param Criteria $criteria
     *
     * @return Collection
     */
    public function getList(Criteria $criteria): Collection
    {
        return $this->matching(
            $criteria
        );
    }

    /**
     * @param Criteria $criteria
     * @throws ORMException
     */
    public function deleteList(Criteria $criteria): void
    {
        $entityManager = $this->getEntityManager();
        $items = $this->matching(
            $criteria
        );
        foreach ($items as $item) {
            $entityManager->remove($item);
        }
        $entityManager->flush();
    }

    /**
     * @throws OptimisticLockException
     * @throws ORMException
     */
    public function createOrUpdate(
        array $data
    ): ResursbankAnnuity {
        if (!$annuity = $this->getDuplicate(
            $data[ResursbankAnnuity::KEY_METHOD_ID],
            $data[ResursbankAnnuity::KEY_DURATION])
        ) {
            $annuity = new ResursbankAnnuity();
        }
        $annuity
            ->setMethodId($data['method_id'])
            ->setDuration($data['duration'])
            ->setFactor($data['factor'])
            ->setTitle($data['title'])
            ->setRaw($data['raw']);
        $this->getEntityManager()->persist($annuity);

        $this->getEntityManager()->flush();
        return $annuity;
    }

    /**
     * @param int $methodId
     * @param int $duration
     * @return ResursbankAnnuity|null
     * @throws ResolveCriteriaException
     */
    protected function getDuplicate(int $methodId, int $duration): ?ResursbankAnnuity
    {
        $criteria = Criteria::create()
            ->where($this->getExp()->eq('methodId', $methodId))
            ->andWhere($this->getExp()->eq(ResursbankAnnuity::KEY_DURATION, $duration));
        return $this->getList($criteria)->count() ? $this->getList($criteria)->first() : null;
    }

    /**
     * @return ExpressionBuilder
     * @throws ResolveCriteriaException
     */
    protected function getExp(): ExpressionBuilder {
        $exp = Criteria::expr();

        if (!$exp instanceof ExpressionBuilder) {
            throw new ResolveCriteriaException('Failed to generate criteria.');
        }

        return $exp;
    }
}
