<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\PartPayment\Config;

use Exception;
use PrestaShop\PrestaShop\Adapter\Configuration as Adapter;
use PrestaShop\PrestaShop\Core\Domain\Shop\ValueObject\ShopConstraint;

/**
 * Interact with the configuration table. This class provide methods to read and
 * write all values related to our module.
 */
class Config
{
    private const CONFIG_PREFIX = 'RESURSBANK_PARTPAYMENT_';
    public const ENABLED = self::CONFIG_PREFIX . 'ENABLED';
    public const METHOD_ID = self::CONFIG_PREFIX . 'METHOD_ID';
    public const ANNUITY_ID = self::CONFIG_PREFIX . 'ANNUITY_ID';
    public const THRESHOLD = self::CONFIG_PREFIX . 'THRESHOLD';

    /**
     * @var Adapter
     */
    protected $adapter;

    /**
     * @param Adapter $adapter
     */
    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
    }

    /**
     * @param ShopConstraint|null $shopConstraint
     *
     * @return string
     */
    public function getEnabled(
        ShopConstraint $shopConstraint = null
    ): string {
        return (string) $this->adapter->get(
            self::ENABLED,
            false,
            $shopConstraint
        );
    }

    /**
     * @param string $value
     * @param ShopConstraint|null $shopConstraint
     *
     * @throws Exception
     */
    public function setEnabled(
        string $value,
        ShopConstraint $shopConstraint = null
    ): void {
        $this->adapter->set(self::ENABLED, $value, $shopConstraint);
    }

    /**
     * @param ShopConstraint|null $shopConstraint
     *
     * @return int
     */
    public function getMethodId(
        ShopConstraint $shopConstraint = null
    ): int {
        return (int) $this->adapter->get(
            self::METHOD_ID,
            false,
            $shopConstraint
        );
    }

    /**
     * @param int $value
     * @param ShopConstraint|null $shopConstraint
     *
     * @throws Exception
     */
    public function setMethodId(
        int $value,
        ShopConstraint $shopConstraint = null
    ): void {
        $this->adapter->set(self::METHOD_ID, $value, $shopConstraint);
    }

    /**
     * @param ShopConstraint|null $shopConstraint
     *
     * @return int
     */
    public function getAnnuityId(
        ShopConstraint $shopConstraint = null
    ): int {
        return (int) $this->adapter->get(
            self::ANNUITY_ID,
            false,
            $shopConstraint
        );
    }

    /**
     * @param int $value
     * @param ShopConstraint|null $shopConstraint
     *
     * @throws Exception
     */
    public function setAnnuityId(
        int $value,
        ShopConstraint $shopConstraint = null
    ): void {
        $this->adapter->set(self::ANNUITY_ID, $value, $shopConstraint);
    }

    /**
     * @param ShopConstraint|null $shopConstraint
     *
     * @return float
     */
    public function getThreshold(
        ShopConstraint $shopConstraint = null
    ): float {
        return (float) $this->adapter->get(
            self::THRESHOLD,
            false,
            $shopConstraint
        );
    }

    /**
     * @param float $value
     * @param ShopConstraint|null $shopConstraint
     *
     * @throws Exception
     */
    public function setThreshold(
        float $value,
        ShopConstraint $shopConstraint = null
    ): void {
        $this->adapter->set(self::THRESHOLD, $value, $shopConstraint);
    }
}
