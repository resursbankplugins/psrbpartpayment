<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\PartPayment\Config\Form;

use Exception;
use JsonException;
use PrestaShop\PrestaShop\Core\Form\FormDataProviderInterface;
use Resursbank\Core\Entity\ResursbankPaymentMethod;
use Resursbank\Core\Exception\PaymentMethodException;
use Resursbank\Core\Logger\Logger;
use Resursbank\Core\Logger\LoggerInterface;
use Resursbank\Core\Service\PaymentMethods;
use Resursbank\PartPayment\Config\Config;
use Resursbank\RBEcomPHP\ResursBank;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Configuration page data provider.
 */
class DataProvider implements FormDataProviderInterface
{
    /**
     * @var Config
     */
    protected $config;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @var TranslatorInterface
     */
    protected $translator;

    /**
     * Resurs Bank payment methods storage.
     *
     * @var PaymentMethods
     *
     * @since 1.0.0
     */
    private $paymentMethods;

    /**
     * @var \psrbpartpayment
     */
    private $module;

    /**
     * @param Config $config
     * @param Logger $logger
     * @param TranslatorInterface $translator
     * @param PaymentMethods $paymentMethods
     */
    public function __construct(
        Config $config,
        LoggerInterface $logger,
        TranslatorInterface $translator,
        PaymentMethods $paymentMethods
    ) {
        $this->config = $config;
        $this->logger = $logger;
        $this->translator = $translator;
        $this->paymentMethods = $paymentMethods;
        $this->module = \Module::getInstanceByName('psrbpartpayment');
    }

    /**
     * @return ResursBank
     *
     * @throws Exception
     */
    private function getApiFunctions(): ResursBank
    {
        if ($this->apiClass instanceof ResursBank) {
            return $this->apiClass;
        }

        return $this->apiClass = new ResursBank();
    }

    /**
     * @inheridoc
     *
     * @return array
     *
     * @throws PaymentMethodException|JsonException
     */
    public function getData(): array
    {
        $methodId = $this->config->getMethodId();
        $methodModel = null;
        $methodList = $this->paymentMethods->getList();
        $minTotal = 0.0;
        $maxTotal = 0.0;

        if ($methodList !== null) {
            /** @var ResursbankPaymentMethod $method */
            foreach ($methodList as $method) {
                if ($method->getId() === $methodId) {
                    $methodModel = $method;
                    break;
                }
            }
        }

        if ($methodModel !== null && $this->showMinMax($method)) {
            $minTotal = $methodModel->getMinOrderTotal();
            $maxTotal = $methodModel->getMaxOrderTotal();
        }

        return [
            'general' => [
                'enabled' => $this->config->getEnabled(),
                'method' => $this->config->getMethodId(),
                'annuity' => $this->config->getAnnuityId(),
                'threshold' => $this->config->getThreshold(),
                'minTotal' => $minTotal,
                'maxTotal' => $maxTotal,
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function setData(
        array $data
    ): array {
        return array_filter([
            $this->setEnabled($data),
            $this->setMethodId($data),
            $this->setAnnuityId($data),
            $this->setThreshold($data),
        ]);
    }

    /**
     * @param array $data
     *
     * @return string
     */
    private function setEnabled(
        array $data
    ): string {
        $error = '';

        try {
            if (isset($data['general']['enabled'])) {
                $this->config->setEnabled($data['general']['enabled']);
            }
        } catch (Exception $e) {
            $this->logger->exception($e);

            $error = $this->module->l('Failed to update enabled state.');
        }

        return $error;
    }

    /**
     * @param array $data
     *
     * @return string
     */
    private function setMethodId(
        array $data
    ): string {
        $error = '';

        try {
            if (isset($data['general']['method'])) {
                $this->config->setMethodId((int) $data['general']['method']);
            }
        } catch (Exception $e) {
            $this->logger->exception($e);

            $error = $this->module->l('Failed to update method id.');
        }

        return $error;
    }

    /**
     * @param array $data
     *
     * @return string
     */
    private function setAnnuityId(
        array $data
    ): string {
        $error = '';

        try {
            if (isset($data['general']['annuity'])) {
                $this->config->setAnnuityId((int) $data['general']['annuity']);
            }
        } catch (Exception $e) {
            $this->logger->exception($e);

            $error = $this->module->l('Failed to update annuity id.');
        }

        return $error;
    }

    /**
     * @param array $data
     *
     * @return string
     */
    private function setThreshold(
        array $data
    ): string {
        $error = '';

        try {
            if (isset($data['general']['threshold'])) {
                $this->config->setThreshold((float) $data['general']['threshold']);
            }
        } catch (Exception $e) {
            $this->logger->exception($e);

            $error = $this->module->l('Failed to update threshold.');
        }

        return $error;
    }

    /**
     * Only show Min & Max for methods that have a type that is not
     * CARD or PAYMENT_PROVIDER.
     *
     * @param ResursbankPaymentMethod $method
     *
     * @return bool
     *
     * @throws JsonException
     */
    private function showMinMax(
        ResursbankPaymentMethod $method
    ): bool {
        return
            $this->getApiFunctions()->isInternalMethod($method->getType()) &&
            $method->getSpecificType() !== 'CARD';
    }
}
