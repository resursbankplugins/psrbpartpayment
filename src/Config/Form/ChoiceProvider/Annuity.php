<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\PartPayment\Config\Form\ChoiceProvider;

use Doctrine\Common\Collections\Criteria;
use Exception;
use PrestaShop\PrestaShop\Core\Form\FormChoiceProviderInterface;
use Resursbank\Core\Logger\LoggerInterface;
use Resursbank\PartPayment\Repository\ResursbankAnnuityRepository;

class Annuity implements FormChoiceProviderInterface
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var ResursbankAnnuityRepository
     */
    private $annuityRepository;

    /**
     * @var array
     */
    protected $annuities = [];

    /**
     * @param LoggerInterface $logger
     * @param ResursbankAnnuityRepository $annuityRepository
     */
    public function __construct(
        LoggerInterface             $logger,
        ResursbankAnnuityRepository $annuityRepository
    ) {
        $this->logger = $logger;
        $this->annuityRepository = $annuityRepository;
    }

    /**
     * @return array
     */
    public function getChoices(): array
    {
        if (!empty($this->annuities)) {
            return $this->annuities;
        }

        try {
            $annuities = $this->annuityRepository
                ->getList(new Criteria());
            $this->annuities = $annuities->toArray();
        } catch (Exception $e) {
            $this->logger->exception($e);
        }
        return $this->annuities;
    }
}
