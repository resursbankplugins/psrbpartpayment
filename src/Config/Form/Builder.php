<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\PartPayment\Config\Form;

use PrestaShop\PrestaShop\Core\Form\FormDataProviderInterface;
use PrestaShopBundle\Service\Routing\Router;
use Resursbank\PartPayment\Config\Form\Builder\General;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;

/**
 * Configuration form builder.
 */
class Builder
{
    /**
     * @var FormFactoryInterface
     */
    protected $formFactory;

    /**
     * @var FormDataProviderInterface
     */
    protected $formDataProvider;

    /**
     * @var Router
     */
    protected $router;

    /**
     * @param FormFactoryInterface $formFactory
     * @param FormDataProviderInterface $formDataProvider
     * @param Router $router
     */
    public function __construct(
        FormFactoryInterface $formFactory,
        FormDataProviderInterface $formDataProvider,
        Router $router
    ) {
        $this->formFactory = $formFactory;
        $this->formDataProvider = $formDataProvider;
        $this->router = $router;
    }

    /**
     * @return FormInterface
     */
    public function getForm(): FormInterface
    {
        return $this->formFactory->createBuilder()
            ->setData($this->formDataProvider->getData())
            ->add('general', General::class)
            ->setAction(
                $this->router->generate(
                    'resursbank_partpayment_admin_config_save'
                )
            )->getForm();
    }
}
