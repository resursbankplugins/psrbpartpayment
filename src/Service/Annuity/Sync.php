<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\PartPayment\Service\Annuity;

use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Collections\ExpressionBuilder;
use Exception;
use Resursbank\Core\Api\Connection;
use Resursbank\Core\Entity\ResursbankPaymentMethod;
use Resursbank\Core\Logger\LoggerInterface;
use Resursbank\PartPayment\Entity\ResursbankAnnuity;
use Resursbank\PartPayment\Exception\IntegrationException;
use Resursbank\PartPayment\Repository\ResursbankAnnuityRepository;
use function is_array;
use Resursbank\Core\Model\Api\Credentials;
use stdClass;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Sync
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var Connection
     */
    private $connection;

    /**
     * @var Converter
     */
    private $converter;
    /**
     * @var ResursbankAnnuityRepository
     */
    private $annuityRepository;


    public function __construct(
        Connection                  $connection,
        Converter                   $converter,
        ResursbankAnnuityRepository $annuityRepository,
        LoggerInterface             $logger
    ) {
        $this->logger = $logger;

        $this->connection = $connection;
        $this->converter = $converter;
        $this->annuityRepository = $annuityRepository;
    }

    /**
     * Updates database entries of annuities for a specific payment method with
     * data fetched from the API.
     *
     * If this process somehow fails for an annuity, an error will be logged and
     * that annuity will be ignored.
     *
     * Returns a list of IDs (integers) that were successfully synced.
     *
     * @param ResursbankPaymentMethod $method
     * @param Credentials $credentials
     * @throws Exception
     * @return array<int>
     */
    public function sync(
        ResursbankPaymentMethod $method,
        Credentials $credentials
    ): array {
        /** @var array<int> $synced */
        $synced = [];

        // Load annuities from the API.
        $annuities = $this->connection
            ->getConnection($credentials)
            ->getAnnuityFactors((string)$method->getIdentifier());

        foreach ($annuities as $annuity) {
            try {
                // New entry from converted data.
                $this->logger->info(print_r($annuity, true));
                $syncedAnnuity = $this->arrayToAnnuity($annuity, $method);

                if ($syncedAnnuity !== null) {
                    $synced[] = $syncedAnnuity->getAnnuityId();
                    $this->logger->info(
                        'Synced annuity ' .
                        '"' . $syncedAnnuity->getRaw() . '"' .
                        'for payment method ' .
                        '"' . $method->getCode() . '"'
                    );
                }
            } catch (Exception $error) {
                $this->logger->exception($error);
            }
        }

        return $synced;
    }

    /**
     * Convert anonymous array to AnnuityInterface instance.
     *
     * @param mixed $data
     * @param ResursbankPaymentMethod $method
     * @return ResursbankAnnuity|null
     */
    public function arrayToAnnuity(
        $data,
        ResursbankPaymentMethod $method
    ): ?ResursbankAnnuity {
        $annuity = null;

        try {
            $annuity = $this->converter->convert(
                $this->resolveAnnuityDataArray($data),
                $method
            );
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());
            $this->logger->exception($e);
        }

        return $annuity;
    }

    /**
     * The data returned from ECom when fetching annuities is described as
     * mixed. We can therefore not be certain what we get back and need to
     * properly convert the data to an array for further processing.
     *
     * @param mixed $data
     * @return array<string, mixed>
     * @throws IntegrationException
     */
    public function resolveAnnuityDataArray(
        $data
    ): array {
        $result = $data;

        if ($data instanceof stdClass) {
            $result = (array) $result;
        }

        if (!is_array($result)) {
            throw new IntegrationException('Unexpected annuity returned from ECom.');
        }

        return $result;
    }

    /**
     * Takes a list of annuity IDs, then removes all entries from the database
     * that do not have any of the given IDs. In other words, all of the
     * IDs in the list represents entries that will not be deleted.
     *
     * @param array<int> $ids
     * @param ResursbankPaymentMethod $method
     * @throws Exception
     */
    public function deleteOldEntries(
        array $ids,
        ResursbankPaymentMethod $method
    ): void {
        $expressionBuilder = new ExpressionBuilder();
        $criteria = Criteria::create()
            ->where($expressionBuilder->eq('methodId', $method->getMethodId()))
            ->andWhere($expressionBuilder->notIn('annuityId', $ids));
        $this->annuityRepository->deleteList($criteria);
    }
}
