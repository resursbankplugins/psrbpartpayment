<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\PartPayment\Service\Annuity;

use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use JsonException;
use Resursbank\Core\Entity\ResursbankPaymentMethod;
use Resursbank\PartPayment\Entity\ResursbankAnnuity;
use Resursbank\PartPayment\Repository\ResursbankAnnuityRepository;
use Symfony\Component\Validator\Exception\ValidatorException;
use function is_int;
use function is_string;

/**
 * Convert annuity data from the Resurs Bank API to data which can be
 * interpreted by our Magento module.
 */
class Converter
{
    /**
     * @var string
     */
    public const KEY_TITLE = 'paymentPlanName';

    /**
     * @var ResursbankAnnuityRepository
     */
    private $annuityRepository;

    /**
     * @param ResursbankAnnuityRepository $annuityRepository
     */
    public function __construct(
        ResursbankAnnuityRepository $annuityRepository
    ) {
        $this->annuityRepository = $annuityRepository;
    }

    /**
     * @param array $data
     * @param ResursbankPaymentMethod $method
     * @return ResursbankAnnuity
     * @throws JsonException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function convert(
        array                   $data,
        ResursbankPaymentMethod $method
    ): ResursbankAnnuity
    {
        // Validate provided data.
        if (!$this->validate($data)) {
            throw new ValidatorException(
                    sprintf('Data conversion failed. Provided data is invalid. %s',
                    json_encode($data, JSON_THROW_ON_ERROR))
            );
        }

        // Validate provided payment method.
        if ($method->getMethodId() === null) {
            throw new ValidatorException('Missing payment method entity.');
        }

        // Convert to data Magento can interpret.
        return $this->annuityRepository->createOrUpdate([
            ResursbankAnnuity::KEY_METHOD_ID => $method->getMethodId(),
            ResursbankAnnuity::KEY_DURATION => $data[ResursbankAnnuity::KEY_DURATION],
            ResursbankAnnuity::KEY_FACTOR => (float)$data[ResursbankAnnuity::KEY_FACTOR],
            ResursbankAnnuity::KEY_TITLE => $data[self::KEY_TITLE],
            ResursbankAnnuity::KEY_RAW => json_encode($data, JSON_THROW_ON_ERROR)
        ]);
    }

    /**
     * @param array<mixed> $data
     * @return bool
     */
    public function validate(
        array $data
    ): bool
    {
        return (
            $this->validateDuration($data) &&
            $this->validateFactor($data) &&
            $this->validateTitle($data)
        );
    }

    /**
     * @param array<mixed> $data
     * @return bool
     */
    public function validateDuration(
        array $data
    ): bool
    {
        return (
            isset($data[ResursbankAnnuity::KEY_DURATION]) &&
            is_int($data[ResursbankAnnuity::KEY_DURATION]) &&
            $data[ResursbankAnnuity::KEY_DURATION] > 0
        );
    }

    /**
     * @param array<mixed> $data
     * @return bool
     */
    public function validateFactor(
        array $data
    ): bool
    {
        return (
            isset($data[ResursbankAnnuity::KEY_FACTOR]) &&
            is_numeric($data[ResursbankAnnuity::KEY_FACTOR]) &&
            (float)$data[ResursbankAnnuity::KEY_FACTOR] > 0.0
        );
    }

    /**
     * @param array<mixed> $data
     * @return bool
     */
    public function validateTitle(
        array $data
    ): bool
    {
        return (
            isset($data[self::KEY_TITLE]) &&
            is_string($data[self::KEY_TITLE]) &&
            $data[self::KEY_TITLE] !== ''
        );
    }
}
