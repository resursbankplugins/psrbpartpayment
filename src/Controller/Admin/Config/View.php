<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\PartPayment\Controller\Admin\Config;

use Exception;
use PrestaShop\PrestaShop\Core\Form\FormChoiceProviderInterface;
use PrestaShopBundle\Controller\Admin\FrameworkBundleAdminController;
use Resursbank\Core\Entity\ResursbankPaymentMethod;
use Resursbank\Core\Logger\LoggerInterface;
use Resursbank\PartPayment\Config\Form\Builder;
use Symfony\Component\HttpFoundation\Response;

/**
 * Render configuration page.
 */
class View extends FrameworkBundleAdminController
{
    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var Builder
     */
    protected $builder;

    /**
     * @var FormChoiceProviderInterface
     */
    protected $methodChoiceProvider;

    /**
     * @var \psrbpartpayment
     */
    private $module;

    /**
     * @param LoggerInterface $logger
     * @param Builder $builder
     * @param FormChoiceProviderInterface $methodChoiceProvider
     */
    public function __construct(
        LoggerInterface $logger,
        Builder $builder,
        FormChoiceProviderInterface $methodChoiceProvider
    ) {
        parent::__construct();
        $this->logger = $logger;
        $this->builder = $builder;
        $this->methodChoiceProvider = $methodChoiceProvider;
        $this->module = \Module::getInstanceByName('psrbpartpayment');
    }

    /**
     * @return Response|null
     *
     * @throws Exception
     */
    public function execute(): ?Response
    {
        try {
            $methods = $this->methodChoiceProvider->getChoices();
            $methodVars = array_map(static function (ResursbankPaymentMethod $method) {
                return [
                    'id' => $method->getMethodId(),
                    'min' => $method->getMinOrderTotal(),
                    'max' => $method->getMaxOrderTotal(),
                ];
            }, $methods);

            return $this->render(
                '@Modules/psrbpartpayment/views/admin/config.twig',
                [
                    'layoutTitle' => $this->module->l('Resurs Bank Part Payment'),
                    'requireAddonsSearch' => true,
                    'enableSidebar' => true,
                    'help_link' => '',
                    'form' => $this->builder->getForm()->createView(),
                    'methodVars' => json_encode($methodVars, JSON_THROW_ON_ERROR),
                ]
            );
        } catch (Exception $e) {
            $this->logger->exception($e);
            throw $e;
        }
    }
}
