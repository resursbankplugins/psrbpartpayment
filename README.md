# Resurs Bank - PrestaShop module - Part Payment

## Description

Part payment functionality for PrestaShop module.

---

## Prerequisites

* [PrestaShop 1.7.7+]

---

#### 1.0.0

* Initial release.
