/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

/**
 * @namespace RbppView
 */

/**
 * Global variable.
 *
 * @typedef {object} psrbpartpayment
 * @property {psrbpartpayment.View} View
 */

/**
 * @typedef {object} psrbpartpayment.View
 * @property {RbcView.ReadMoreFn} ReadMore
 */

if (window.psrbpartpayment === undefined) {
    window.psrbpartpayment = {};
}

if (window.psrbpartpayment.View === undefined) {
    window.psrbpartpayment.View = {};
}
