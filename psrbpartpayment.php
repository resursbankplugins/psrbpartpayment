<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

/* Start our custom atuoloader. We must start it at this point for the module
 manager to function. */
require_once(_PS_MODULE_DIR_ . "/psrbcore/src/Service/Autoloader.php");

use Resursbank\Core\Exception\InstallerException;
use Resursbank\Core\Exception\MissingPathException;
use Resursbank\Core\Logger\Logger;
use Resursbank\Core\ModuleInterface;
use Resursbank\Core\Service\Sql;
use Resursbank\Core\Service\Tab as ResursTab;
use Resursbank\Core\Traits\Module\Init;
use Resursbank\Core\Traits\Module\Validate;
use Resursbank\PartPayment\Config\Config;
use Resursbank\PartPayment\Observer\AddReadMoreToProductPage;
use Resursbank\PartPayment\Observer\UpdateAnnuities;
use TorneLIB\Exception\ExceptionHandler;

if (!defined('_PS_VERSION_')) {
    exit;
}

/**
 * Part payment module main class.
 */
class psrbpartpayment extends Module implements ModuleInterface
{
    use Init;
    use Validate;

    /**
     * List of hooks (event observers) to register.
     *
     * @var string[]
     */
    private $hooks = [
        'displayProductAdditionalInfo',
        'resursBankAfterPaymentMethodSync',
        'actionFrontControllerSetMedia',
    ];

    /**
     * @throws ExceptionHandler
     * @throws ReflectionException
     * @throws MissingPathException
     */
    public function __construct()
    {
        $this->init($this, 1);
        parent::__construct();
    }

    /**
     * @throws Exception
     */
    public function install(): bool
    {
        try {
            // This module cannot function without psrbcore.
            $this->validateCore();

            if (!parent::install()) {
                throw new Exception('Parent installation procedure failed.');
            }

            // Install tables.
            Sql::exec('psrbpartpayment', 'install.sql');

            // Configs not loaded yet, cannot access class through container.
            $tab = ResursTab::getInstance($this);

            // Config page tab.
            $tab->create(
                $this->getName(),
                'ResursBankPartPayment',
                'ResursBank',
                'Part Payment',
                'resursbank_partpayment_admin_config',
                'Modules.ResursBank.PartPayment.Tab'
            );

            // Disable module by default.
            Configuration::updateValue(Config::ENABLED, '1', false, 0, 0);
        } catch (Exception $e) {
            throw new InstallerException($e->getMessage());
        }

        return true;
    }

    /**
     * @throws Exception
     */
    public function uninstall(): bool
    {
        try {
            if (!parent::uninstall()) {
                throw new Exception('Parent uninstall procedure failed.');
            }

            // Delete config data.
            Configuration::deleteByName(Config::ENABLED);

            // Configs not loaded yet, cannot access class through container.
            $tab = ResursTab::getInstance($this);
            $tab->delete('ResursBankPartPayment');
        } catch (Exception $e) {
            throw new InstallerException($e->getMessage());
        }

        return true;
    }

    /**
     * @param bool $force_all
     *
     * @return bool
     *
     * @throws InstallerException
     */
    public function enable($force_all = false): bool
    {
        try {
            // This module cannot function without psrbcore.
            $this->validateCore();

            if (!parent::enable($force_all)) {
                throw new Exception('Parent enable procedure failed.');
            }

            // Configs not loaded yet, cannot access class through container.
            $tab = ResursTab::getInstance($this);
            $tab->activate('ResursBankPartPayment');

            // Register event observers.
            $this->registerHook($this->hooks);
        } catch (Exception $e) {
            throw new InstallerException($e->getMessage());
        }

        return true;
    }

    /**
     * @param bool $force_all
     *
     * @return bool
     *
     * @throws InstallerException
     */
    public function disable($force_all = false): bool
    {
        try {
            if (!parent::disable($force_all)) {
                throw new Exception('Parent disable procedure failed.');
            }

            // Configs not loaded yet, cannot access class through container.
            $tab = ResursTab::getInstance($this);
            $tab->deactivate('ResursBankPartPayment');

            // Unregister hooks (event observers).
            foreach ($this->hooks as $hook) {
                $this->unregisterHook($hook);
            }
        } catch (Exception $e) {
            throw new InstallerException($e->getMessage());
        }

        return true;
    }

    /**
     * @param $params
     *
     * @return string
     * @noinspection PhpUnused
     */
    public function hookDisplayProductAdditionalInfo($params): string
    {
        $result = '';

        try {
            /** @var AddReadMoreToProductPage $observer */
            $observer = $this->get(
                'resursbank.partpayment.observer.add.read.more.product.page'
            );

            $result = $observer->execute($this, $params);
        } catch (Exception $e) {
            // Do nothing.
        }

        return $result;
    }

    /**
     * Add "Read More" JS / CSS to product pages.
     *
     * @return void
     *
     * @throws Exception
     * @noinspection PhpUnused
     */
    public function hookActionFrontControllerSetMedia(): void
    {
        $self = $this->context->controller->php_self;

        if ($self === 'product') {
            $this->context->controller->registerJavascript(
                $this->name . '-init',
                $this->getPathUri() . 'js/psrbpartpayment.js',
            );

            $this->context->controller->registerJavascript(
                $this->name . '-model-part-payment',
                $this->getPathUri() . 'js/model/part-payment.js',
            );

            $this->context->controller->registerJavascript(
                $this->name . '-lib-part-payment',
                $this->getPathUri() . 'js/lib/part-payment.js',
            );

            $this->context->controller->registerJavascript(
                $this->name . '-view-read-more',
                $this->getPathUri() . 'js/view/read-more.js',
            );

            psrbcore::addRemodalAssets(
                $this->name,
                $this->getPathUri(),
                $this->context->controller
            );

            psrbcore::addReadMoreAssets(
                $this->name,
                $this->getPathUri(),
                $this->context->controller
            );
        }
    }

    /**
     * @param string $key
     * @param array $params
     *
     * @return string
     *
     * @since 1.0.0
     */
    public function getModuleLink(string $key, array $params = []): string
    {
        return $this->context->link->getModuleLink(
            $this->name,
            $key,
            $params,
            true
        );
    }

    /**
     * @param mixed $params
     *
     * @return void
     * @noinspection PhpUnused
     *
     * @throws PrestaShopException
     */
    public function hookResursBankAfterPaymentMethodSync($params): void
    {
        try {
            /** @var UpdateAnnuities $updateAnnuities */
            $updateAnnuities = $this->get(
                'resursbank.partpayment.observer.after.payment.method.sync'
            );
            $updateAnnuities->execute($params);
        } catch (Exception $e) {
            $this->getLog()->exception($e);
        }
    }

    /**
     * @return string
     *
     * @since 1.0.0
     */
    public function getVersion(): string
    {
        return $this->version;
    }

    /**
     * @return string
     *
     * @since 1.0.0
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Declares we intend to use the new interface to translate our module.
     *
     * @return bool
     * @noinspection PhpMissingParentCallCommonInspection
     */
    public function isUsingNewTranslationSystem(): bool
    {
        return true;
    }

    /**
     * @return Logger
     *
     * @throws PrestaShopException
     */
    private function getLog(): Logger
    {
        try {
            $logger = $this->get('resursbank.partpayment.logger');

            if (!$logger instanceof Logger) {
                throw new PrestaShopException('Failed to resolve logger.');
            }
        } catch (Exception $e) {
            throw new PrestaShopException($e->getMessage());
        }

        return $logger;
    }
}
