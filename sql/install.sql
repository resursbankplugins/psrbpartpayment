CREATE TABLE IF NOT EXISTS `PREFIX_resursbank_annuity` (
    `annuity_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Prim Key',
    `method_id` smallint(5) unsigned NOT NULL COMMENT 'Payment method id',
    `duration` smallint(5) unsigned NOT NULL COMMENT 'Duration',
    `factor` decimal(15,5) unsigned NOT NULL COMMENT 'Annuity factor',
    `title` varchar(255) NOT NULL COMMENT 'Title',
    `raw` text DEFAULT NULL COMMENT 'Raw API Data',
    `created_at` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'Created At',
    `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'Updated At',
    PRIMARY KEY (`annuity_id`)
    ) CHARSET=utf8 COMMENT='Resurs Bank Annuity Table'
